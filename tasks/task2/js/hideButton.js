function hideButton() {
  var buttonToHide = document.getElementById("button-to-hide");
  var buttonToRepeat = document.getElementById("button-to-repeat");
  var isButtonToHideVisible = (buttonToHide.style.display !== "none");
  
  if (isButtonToHideVisible) {
    buttonToHide.style.display = "none";
    buttonToRepeat.style.display = "inline-block";
  }
  else if (!isButtonToHideVisible) {
    buttonToHide.style.display = "inline-block";
    buttonToRepeat.style.display = "none";
  }
}
