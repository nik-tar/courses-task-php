<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>#1: Show Source Code</title>
  <link href="themes/prism.css" rel="stylesheet" />
  <script defer src="themes/prism.js"></script>
</head>
<body>
  <?php
    $filename = basename($_SERVER['PHP_SELF']);
    $code = file_get_contents($filename);
    $escapedCode = htmlspecialchars($code);
    echo '<pre><code class="language-php language-markup">' . $escapedCode . '</code></pre>';
  ?>
</body>
</html>
