#!/usr/bin/php
<?php
  // returns "sample_name" only if the string was empty
  // or if it has been filled in by unallowed characters
  function filenameParser($filename) {
    if (strlen($filename) == 0) {
      return "sample_name";
    }
    else {
      $filename = strtolower($filename);
      $dotDelete = explode(".", $filename);
      $extention = "";
      // delete the last dot if it's in filename
      if ($dotDelete[count($dotDelete) - 1] == '') {
        array_splice($dotDelete, -1);
      }

      $dotDelete = preg_replace('([\\\/ ]+)', '_', $dotDelete);
      $dotDelete = preg_replace('([^\w\d_]+)', '', $dotDelete);

      if (count($dotDelete) == 2 && $dotDelete[0] == '') {
        // only one dot at the beggining, filter $dotDelete[1] there
        $filename = join('.', $dotDelete);
      }
      else {
        $extention = array_pop($dotDelete);
        $extention = preg_replace('([_]+)', '', $extention);
        $filename = join('', $dotDelete);
        $filename = preg_replace('([_]+)', '_', $filename);
      }

      // check length of extention
      if (strlen($extention) > 50) {
        $extArray = str_split($extention);
        for ($i = count($extArray) - 1; $i > 0; $i--) {
          if (!strcmp($extArray[$i], $extArray[$i - 1])) {
            unset($extArray[$i]);
          }
        }
        $extention = join('', $extArray);
        if (strlen($extention) > 50) {
          $extention = substr($extention, 0, 50);
        }
      }

      $result = $filename . "." . $extention;

      // check lenght of a result
      if (strlen($result) > 200) {
        $extArray = str_split($filename);
        for ($i = count($extArray) - 1; $i > 0; $i--) {
          if (!strcmp($extArray[$i], $extArray[$i - 1])) {
            unset($extArray[$i]);
          }
        }
        $filename = join('', $extArray);
        $temp = strlen($filename . "." . $extention);
        if ($temp > 200) {
          $filename = substr($filename, 0, 200 - strlen($extention));
        }
        $result = $filename . "." . $extention;
      }

      $finalResult = !strcmp($filename[0], ".") ? $filename : $result;
      return !strcmp($finalResult, "_") ? "sample_name" : $finalResult;
    }
  }

  if (count($argv) > 2) {
    echo "Please, type one file name and type it in quotes.\n";
  }
  else {
    echo filenameParser($argv[1]) . "\n";
  }
?>
